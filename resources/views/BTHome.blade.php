<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BHUTAN TELECOM LIMITED</title>
    <link href='https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css' rel='stylesheet'>
</head>
<style>
    body {
        margin: 0;
        padding: 0;
        font-family: 'Open Sans', sans-serif;
    }

    .main {
        display: flex;
        justify-content: center;
        flex-direction: column;
    }

    .header {
        display: flex;
        justify-content: space-between;
        flex-direction: row;
        width: 100%;
        padding-left: 80px;
        padding-right: 90px;
        border-bottom: 1px solid rgb(211, 211, 210);
        box-sizing: border-box; /* Include padding in the total width */
    }

    .header_1,
    .header_2 {
        display: flex;
        justify-content: center;
        flex-direction: row;
    }

    .header_1 p,
    .header_2 p {
        margin-right: 25px;
        padding: 5px;
    }

    .webmail {
        border: 1px solid hsl(100, 56%, 49%);
        padding: 5px;
        border-radius: 5px;
    }

    .webmail:hover {
        background: hsl(100, 56%, 49%);
        color: rgb(248, 248, 248);
    }
    .logo{
        display: flex;
        flex-direction: row;
        justify-content: space-between;
        width: 100%;
        padding-left: 45px;
        padding-right: 90px;
        
        box-sizing: border-box; 

    }
    .logo_img {
        display: flex;
        justify-content: center;
        flex-direction: row;
    }
    
    .logo_img img{
        max-width: 75%;
        height: auto;
    
        
    }
    .logo_icon {
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: row;
        
    }
    .logo_icon a{
       display: flex;
       justify-content: space-between;
       
       align-items: center;
       text-decoration: none;
       height: 40%;
       margin: 5px;
       border-right: 1px solid rgb(211, 211, 210);
       padding: 5px;
       

    }
    .logo_icon a i{
        font-size: 30px;
        color: hsl(100, 56%, 49%);
    }
    .logo_icon a p{
        display: flex;
        justify-content: center;
        flex-direction: column;
        color: black;
        font-size: 12px;
        padding: 3px;
        
    }
    .navbar{
        display: flex;
        
        background: #6cbc44;
        align-items: center;
        flex-direction: row;
        padding-left: 50px;
    }
    .navbar p{
        margin: 25px;
        color:rgba(240, 240, 240, 0.874);
    }
    .home_img{
        display: flex;
        width: 100%;

    }
    .bt_service{
        display: flex;
        background: #ebf3e3;
    }
    .bt_service h1{
       margin-left: 50px;
    }
    .news{
        display: flex;
        justify-content: center;
        flex-direction: column;
        align-items: center;

    }
    .news h1{
       
        color: #6cbc44;
    }
    .news_detail{
        display:flex;
        justify-content: center;
        width: calc(100%);
        flex-direction: row;
        align-items: center;
        
    }
    .news_img{
        display: flex;
        width: calc(50%);
        justify-content: center;
        align-items: center;
       
    }
    .detail{
        width: calc(50%);
        
        justify-content: center;
        align-items: center;
        text-align: center;

    }
    df-messenger{
        --df-messenger-bot-message: #fff;
    --df-messenger-button-titlebar-color: #42a5f5;
    --df-messenger-button-titlebar-font-color: #fff;
    --df-messenger-chat-background-color: #fafafa;
    --df-messenger-font-color: rgba(0,0,0,.87);
    --df-messenger-input-box-color: #fff;
    --df-messenger-input-font-color: rgba(0,0,0,.87);
    --df-messenger-input-placeholder-font-color: #757575;
    --df-messenger-minimized-chat-close-icon-color: rgba(0,0,0,.87);
    --df-messenger-send-icon: #42a5f5;
    --df-messenger-user-message: #ddd;
    --df-messenger-chip-color: #fff;
    --df-messenger-chip-border-color: #e0e0e0;
       
    }

    
</style>


<body>
    <div class="main">
        <div class="header">
            <div class="header_1"> 
                <p>ENGLISH</p>
                <p>FAQ</p>
                <p>DISCLAIMER</p>
            </div>
            <div class="header_2"> 
                <p>ENGLISH</p>
                <p>FAQ</p>
                <p class="webmail">WEBMAIL</p>
                <p>PIT/BIT</p>
                <p><a href=""><i class='bx bxl-facebook-circle' ></i></a></p>
                <p><a href=""><i class='bx bxl-instagram' ></i></a></p>
                <p><a href="#"><i class='bx bxl-telegram' ></i></a></p>
                
            </div>

           
        </div>
        <div class="logo">
            <div class="logo_img">
                <img src="{{ asset('assets/img/btllogo.png') }}" alt="">
            </div>
            <div class="logo_icon">
                <a href="">
                    <i class='bx bxl-facebook-circle' ></i><p style="font-weight:bold; ">Troll Free</p><br><p>1770</p>
                    
                <a href="">
                    <i class='bx bxl-instagram' ></i><p style="font-weight:bold; ">Troll Free</p><br><p>1770</p>
                    
                </a>
                <a href="">
                    <i class='bx bxl-telegram' ></i><p style="font-weight:bold; ">Troll Free</p><br><p>1770</p>
                    
                </a>
            </div>
        </div>
        <div class="navbar">
            <p>HOME</p>
            <p>ABUOT</p>
            <p>BT-SERVICE</p>
            <p>ONLINE-SERVICE</p>
            <p>BLOG</p>
            <p>BT-VIDEOS</p>
            <p>DOWNLOAD</p>
            <p>CONTACT US</p>
            <button style="border: 3px solid rgb(228, 226, 226); background:hsl(100, 56%, 49%);border-radius:40px;padding:10px; color:rgb(236, 233, 233);">My-BT</button>
            <i style="margin: 10px;color:#f2f2f2;font-size:20px;" class='bx bx-search'></i>
        </div>
        <div class="home_img">
            <img style="height: 50%; max-width: 100%;" src="{{ asset('assets/img/bt_home.png') }}" alt="">
        </div>
        <div class="bt_service">
            <h1>BT SERVICES</h1>
        </div>
        <div class="news">
            
               <h1>News</h1>
                <div class="news_detail">
                    <div class="news_img">
                        <img style="width: 70%;" src="{{ asset('assets/img/news.png') }}" alt="">
                    </div>
                    <div class="detail">
                         <h3> Selected Candidate</h3>
                         <p>BTL(Bhutan Telecom Limited)</p>
                         <button style="width:auto;background: rgb(255, 153, 0);color:white;border-radius:2px;padding:15px; border: none;">LEARN MORE</button>
                         <button  style="width:auto;border:2px solid rgb(239, 237, 237);background:none;color:black;border-radius:2px;padding:15px;">VIEW MORE</button>
                    </div>
                </div>
           
        </div>
        <div>
            <img style="height: 50%; max-width: 100%; " src="{{ asset('assets/img/footer.png') }}" alt="">
        </div>
    </div>
    <script src="https://www.gstatic.com/dialogflow-console/fast/messenger/bootstrap.js?v=1"></script>
<df-messenger
  intent="WELCOME"
  chat-title="BT_Chatbot"
  agent-id="3d2bda16-f60c-484c-a5d2-eacbcf9cc99e"
  language-code="en"
  
  
></df-messenger>

</body>
</html>